<?php
$doc = new DOMDocument;
$content_rss = file_get_contents('https://www.liga.net/news/society/rss.xml');
$items_rss = new SimpleXMLElement($content_rss);
$enclosure = $doc->getElementsByTagName('enclosure');
foreach ($items_rss->channel->item as $key => $item_rss) {
    echo '<div class="news__item">';

    foreach ($item_rss->enclosure->attributes() as $name => $value) {
        if ($name == 'url') {
            $image = (string)$value;
            echo '<img src="'.$image.'">';
        }
    }
    echo '<h2>' . $item_rss->title . '</h2>';
    echo '<a href="' . $item_rss->link . '">Ссылка на новость</a>';
    echo '</div>';
}